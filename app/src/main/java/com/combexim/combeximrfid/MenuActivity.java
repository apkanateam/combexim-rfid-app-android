package com.combexim.combeximrfid;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.Toast;

public class MenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final View viewDescargar = findViewById(R.id.view_descargar);
        final View viewEscanear = findViewById(R.id.view_escanear);
        final View viewSincronizar = findViewById(R.id.view_Sincronizar);
        final View viewAyuda = findViewById(R.id.view_Ayuda);

        viewDescargar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String welcome = "Descargar datos!";
                Toast.makeText(getApplicationContext(), welcome, Toast.LENGTH_LONG).show();
            }
        });

        viewEscanear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (v.getContext(), EscanerActivity.class);
                startActivityForResult(intent, 0);
            }
        });

        viewSincronizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (v.getContext(), SincronizarActivity.class);
                startActivityForResult(intent, 0);
            }
        });

        viewAyuda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (v.getContext(), AyudaActivity.class);
                startActivityForResult(intent, 0);
            }
        });
    }

}
